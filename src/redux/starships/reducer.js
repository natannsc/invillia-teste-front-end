import { FETCH_STARSHIPS, CLEAR_STARSHIPS } from "./constants";

const initialState = {
  loading: false,
  starships: [],
  error: null,
};

export default function starshipsReducer(state = initialState, action) {
  switch (action.type) {
    case `${FETCH_STARSHIPS}_INIT`:
      return {
        ...state,
        loading: true,
      };
    case `${FETCH_STARSHIPS}_SUCCESS`:
      const starships = [...state.starships, action.data];
      return {
        ...state,
        loading: false,
        starships,
      };
    case `${FETCH_STARSHIPS}_ERROR`:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    case CLEAR_STARSHIPS:
      return {
        ...state,
        loading: false,
        error: null,
        starships: [],
      };
    default:
      return state;
  }
}
