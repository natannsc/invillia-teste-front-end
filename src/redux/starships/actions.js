import axios from "axios";
import { FETCH_STARSHIPS, CLEAR_STARSHIPS } from "./constants";

export const clearStarships = () => ({
  type: CLEAR_STARSHIPS,
});

export const fetchStarships = (urls) => {
  return async (dispatch) => {
    dispatch({
      type: `${FETCH_STARSHIPS}_INIT`,
    });

    try {
      const starships = (
        await Promise.all(
          urls.map((url) => axios.get(url).catch((error) => null))
        )
      )
        // .filter((response) => response && response.ok)
        // .map((response) => response.data);
debugger
      dispatch({
        type: `${FETCH_STARSHIPS}_SUCCESS`,
        data: starships,
      });
    } catch (error) {
      dispatch({
        type: `${FETCH_STARSHIPS}_ERROR`,
        error: error.message,
      });
    }
  };
};
