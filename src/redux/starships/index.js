import starshipsReducer from "./reducer";
import * as starshipsActions from "./actions";
import * as starshipsConstants from "./constants";

export { starshipsReducer, starshipsActions, starshipsConstants };
