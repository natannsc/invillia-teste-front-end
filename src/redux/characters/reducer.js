import { FETCH_CHARACTERS, SELECT_CHARACTER } from "./constants";

const initialState = {
  loading: false,
  characters: [],
  error: null,
  selected: null,
};

export default function charactersReducer(state = initialState, action) {
  switch (action.type) {
    case `${FETCH_CHARACTERS}_INIT`:
      return {
        ...state,
        loading: true,
      };
    case `${FETCH_CHARACTERS}_SUCCESS`:
      const characters = [...state.characters, action.data];
      return {
        ...state,
        loading: false,
        characters,
      };
    case `${FETCH_CHARACTERS}_ERROR`:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    case SELECT_CHARACTER:
      return {
        ...state,
        selected: action.data,
      };
    default:
      return state;
  }
}
