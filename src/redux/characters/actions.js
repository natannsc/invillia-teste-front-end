import axios from "axios";
import { FETCH_CHARACTERS, SELECT_CHARACTER } from "./constants";
import { API_URL } from "../../constants";
// import { fetchStarships, clearStarships } from "../starships/actions";

export const selectCharacter = (character) => {
  return (dispatch) => {
    dispatch({
      type: SELECT_CHARACTER,
      data: character,
    });
    // if (character) {
    //   dispatch(fetchStarships());
    // } else {
    //   dispatch(clearStarships());
    // }
  };
};

export const fetchCharacters = (page = 1) => {
  return async (dispatch) => {
    dispatch({
      type: `${FETCH_CHARACTERS}_INIT`,
    });

    try {
      const { data } = await axios.get(`${API_URL}people?page=${page}`);

      dispatch({
        type: `${FETCH_CHARACTERS}_SUCCESS`,
        data,
      });
    } catch (error) {
      dispatch({
        type: `${FETCH_CHARACTERS}_ERROR`,
        error: error.message,
      });
    }
  };
};
