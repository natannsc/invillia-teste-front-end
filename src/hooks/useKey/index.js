import { useEffect, useState } from 'react';

export const KEYCODE_ENTER = 13;
export const KEYCODE_ESC = 27;

export const usekey = key => {
    const [pressed, setPressed] = useState(false);

    const match = event => key === event.keyCode;

    const onDown = event => {
        if (match(event)) setPressed(true);
    };

    const onUp = event => {
        if (match(event)) setPressed(false);
    };

    useEffect(() => {
        window.addEventListener('keydown', onDown);
        window.addEventListener('keyup', onUp);
        return () => {
            window.removeEventListener('keydown', onDown);
            window.removeEventListener('keyup', onUp);
        };
    }, [key]);

    return pressed;
};

export default usekey;