import React from "react";
import { createGlobalStyle } from "styled-components";
import { hot } from "react-hot-loader/root";
import {
  ThemeProvider,
  createTheme,
  Arwes,
  SoundsProvider,
  createSounds,
  Logo,
} from "arwes";
import { useSelector } from "react-redux";

// Import assets
import "modern-normalize/modern-normalize.css";
import Stars from "./components/Stars";
import Header from "./components/Header";
import Characters from "./components/Characters";
import Loading from "./components/Loading";

// Global Style
const GlobalStyle = createGlobalStyle`
  body {
    background-color: black;
    margin: 0;
    height: 100vh;
    overflow: hidden;
  }
`;

// Main page
const App = () => {
  const { characters } = useSelector((state) => state.charactersReducer);

  return (
    <Arwes animate Puffs={() => null}>
      <Stars />
      <GlobalStyle />
      <Loading animate show={!characters.length} />
      {characters.length ? (
        <>
          <Header />
          <Characters />
        </>
      ) : null}
    </Arwes>
  );
};

export default hot(App);
