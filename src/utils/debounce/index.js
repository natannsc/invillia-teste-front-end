export default (fn, wait) => {
  let t;
  return () => {
    clearTimeout(t);
    t = setTimeout(() => fn.apply(this, arguments), wait);
  };
};
