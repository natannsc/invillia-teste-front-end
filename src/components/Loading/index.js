import React from "react";
import styled from "styled-components";
import { Loading } from "arwes";

const StyledLoading = styled(Loading)`
  position: fixed;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
`;

export default (props) => <StyledLoading {...props} />;
