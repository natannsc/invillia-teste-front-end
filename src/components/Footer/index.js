import React from "react";
import styled from "styled-components";
import { Content, Footer, Header, Heading } from "arwes";

const StyledFooter = styled(Footer)`
  position: fixed;
  bottom: 0;
  width: 100%;
`;

const StyledHeading = styled(Heading)`
  padding: 10px;
  margin-bottom: 0 !important;
`;

export default () => {
  return (
    <StyledFooter animate show>
      <StyledHeading node="h3">Natan dos Santos Camargos</StyledHeading>
    </StyledFooter>
  );
};
