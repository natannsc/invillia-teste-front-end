import React from "react";
import Star from "../Star";

const STARS_COUNT = 200;

export default () => {
  const stars = Array.from({ length: STARS_COUNT }).map(() => <Star />);

  return stars;
};
