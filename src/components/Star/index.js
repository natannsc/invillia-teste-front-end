import React, { useMemo } from "react";
import styled from "styled-components";
import useWindowSize from "../../hooks/useWindowSize";

const StyledStar = styled.div`
  position: fixed;
  width: ${(props) => props.radius}px;
  height: ${(props) => props.radius}px;
  background: white;
  left: ${(props) => Math.round(props.x * props.windowSize.width)}px;
  top: ${(props) => Math.round(props.y * props.windowSize.height)}px;
  border-radius: 50%;
`;

export default () => {
  const [attributes] = React.useState({
    radius: Math.random() * 2,
    x: Math.random(),
    y: Math.random(),
  });

  const windowSize = useWindowSize();

  return useMemo(
    () => <StyledStar {...attributes} windowSize={windowSize} />,
    []
  );
};
