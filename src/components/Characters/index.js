import React, { useEffect } from "react";
import styled from "styled-components";
import {
  Project,
  Row,
  Col,
  Frame,
  Paragraph,
  Content,
  Table,
  Header,
} from "arwes";
import { SpaceShip } from "@styled-icons/remix-line/SpaceShip";
import { Car } from "@styled-icons/boxicons-solid/Car";
import { User } from "@styled-icons/boxicons-solid/User";
import { BirthdayCake } from "@styled-icons/fa-solid/BirthdayCake";
import { CameraMovie } from "@styled-icons/boxicons-regular/CameraMovie";
import { ResizeHeight } from "@styled-icons/open-iconic/ResizeHeight";
import { WeightHanging } from "@styled-icons/fa-solid/WeightHanging";
import { Close } from "@styled-icons/evil/Close";
import { useSelector, useDispatch } from "react-redux";
import { charactersActions } from "../../redux/characters";
import usekey, { KEYCODE_ESC } from "../../hooks/usekey";

const StyledContainer = styled.div`
  margin: 36px 10px 86px;
`;

const StyledClose = styled(Close)`
  position: absolute;
  top: 20px;
  right: 20px;
  width: 24px;
  cursor: pointer;
`;

const StyledParagraph = styled(Paragraph)`
  font-size: 14px;
  margin-bottom: 4px;
  display: flex;
  justify-content: space-between;

  & > * {
    display: flex;

    svg {
      width: 18px;
      max-height: 18px;
    }
  }
`;

const StyledCharacterContent = styled(Content)`
  display: flex;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  padding: 20px;
  z-index: 100;
  background: rgba(0, 0, 0, 0.8);
  overflow: auto;

  display: ${(props) => (props.show ? "initial" : "none")};

  ${StyledParagraph} {
    font-size: 18px;
  }
`;

const StyledCharacterContentSection = styled(Content)`
  @media (max-width: 992px) {
    margin-top: 20px;
  }
`;

const StyledDetailsCol = styled(Col)`
  padding-top: 10px;
  padding-left: 0;

  &:nth-child(n + 2) {
    padding-right: 20px;
  }
`;

export const StyledCharacterProject = styled(Project)``;

export const StyledCharacterFrame = styled(Frame)``;

const StyledCharactersCol = styled(Col)`
  margin-top: 10px;
  margin-bottom: 10px;
  cursor: pointer;

  header span {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    width: 100%;
  }
`;

export const StyledSpan = styled.span`
  font-size: 18px;
`;

export const StyledCharactersFrame = styled(Frame)`
  margin-top: 10px;
  margin-bottom: 10px;
  height: 100%;
  padding: 20px;
`;

export default () => {
  const { characters, selected } = useSelector(
    (state) => state.charactersReducer
  );
  const escPressed = usekey(KEYCODE_ESC);
  const dispatch = useDispatch();

  const normalizedCharacters = characters.reduce(
    (p, c) => [...p, ...c.results],
    []
  );

  useEffect(() => {
    if (escPressed) {
      dispatch(charactersActions.selectCharacter(null));
    }
  }, [escPressed]);

  useEffect(() => {
    if (selected) {
        document.body.style.overflow = 'hidden';
    } else {
        document.body.style.overflow = '';
    }
}, [selected]);

  return (
    <StyledContainer animate>
      <StyledCharacterContent animate show={!!selected}>
        <StyledCharacterProject
          animate
          header={(selected && selected.name) || ""}
        >
          {(anim) =>
            selected && (
              <Row>
                <StyledClose
                  onClick={() =>
                    dispatch(charactersActions.selectCharacter(null))
                  }
                />
                <Col l={5} s={12}>
                  <StyledCharacterContentSection>
                  <Header>Details</Header>
                  <StyledDetailsCol m={6} s={12}>
                    <StyledParagraph animate show={anim}>
                      <span>
                        <User />
                        &nbsp; Gender
                      </span>
                      <span>{selected.gender}</span>
                    </StyledParagraph>
                  </StyledDetailsCol>
                  <StyledDetailsCol m={6} s={12}>
                    <StyledParagraph animate show={anim}>
                      <span>
                        <ResizeHeight />
                        &nbsp; Height
                      </span>
                      <span>{selected.height} cm</span>
                    </StyledParagraph>
                  </StyledDetailsCol>
                  <StyledDetailsCol m={6} s={12}>
                    <StyledParagraph animate show={anim}>
                      <span>
                        <WeightHanging />
                        &nbsp; Mass
                      </span>
                      <span>{selected.mass} Kg</span>
                    </StyledParagraph>
                  </StyledDetailsCol>
                  <StyledDetailsCol m={6} s={12}>
                    <StyledParagraph animate show={anim}>
                      <span>
                        <BirthdayCake />
                        &nbsp; Birth year
                      </span>
                      <span>{selected.birth_year}</span>
                    </StyledParagraph>
                  </StyledDetailsCol>
                  <StyledDetailsCol m={6} s={12}>
                    <StyledParagraph animate show={anim}>
                      <span>
                        <SpaceShip />
                        &nbsp; Starships
                      </span>
                      <span>{selected.starships.length}</span>
                    </StyledParagraph>
                  </StyledDetailsCol>
                  <StyledDetailsCol m={6} s={12}>
                    <StyledParagraph animate show={anim}>
                      <span>
                        <Car />
                        &nbsp; Vehicles
                      </span>
                      <span>{selected.vehicles.length}</span>
                    </StyledParagraph>
                  </StyledDetailsCol>
                  <StyledDetailsCol m={6} s={12}>
                    <StyledParagraph animate show={anim}>
                      <span>
                        <CameraMovie />
                        &nbsp; Films
                      </span>
                      <span>{selected.films.length}</span>
                    </StyledParagraph>
                  </StyledDetailsCol>
                  </StyledCharacterContentSection>
                </Col>
                <Col l={7} s={12}>
                  <StyledCharacterContentSection animate>
                    <Header>Starships</Header>
                    {selected.starships.length
                      ? selected.starships.map((starship) => (
                          <StyledDetailsCol m={6} s={12}>
                            <Table
                              animate
                              headers={[starship.starship_class, "", ""]}
                              dataset={[
                                ["MGLT", starship.MGLT],
                                [
                                  "Hyper drive rating",
                                  starship.hyperdrive_rating,
                                ],
                              ]}
                            />
                          </StyledDetailsCol>
                        ))
                      : "--"}
                  </StyledCharacterContentSection>
                </Col>
              </Row>
            )
          }
        </StyledCharacterProject>
      </StyledCharacterContent>
      <Row>
        {normalizedCharacters.map((character) => (
          <StyledCharactersCol
            xl={4}
            l={6}
            s={12}
            title={character.name}
            key={character.name}
            onClick={() =>
              dispatch(charactersActions.selectCharacter(character))
            }
          >
            <Project animate header={character.name}>
              {(anim) => (
                <>
                  <StyledParagraph animate show={anim}>
                    <span>
                      <SpaceShip />
                      &nbsp; Starships
                    </span>
                    <span>{character.starships.length}</span>
                  </StyledParagraph>
                  <StyledParagraph animate show={anim}>
                    <span>
                      <Car />
                      &nbsp; Vehicles
                    </span>
                    <span>{character.vehicles.length}</span>
                  </StyledParagraph>
                </>
              )}
            </Project>
          </StyledCharactersCol>
        ))}
      </Row>
    </StyledContainer>
  );
};
