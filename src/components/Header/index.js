import React from "react";
import styled from "styled-components";
import { Content, Heading, Header } from "arwes";

const StyledHeading = styled(Heading)`
  padding: 10px;
  margin-bottom: 0 !important;
`;

export default () => {
  return (
    <Header animate>
      <StyledHeading node="h1">Invillia - Front End Test</StyledHeading>
    </Header>
  );
};
