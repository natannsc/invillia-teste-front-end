import React from "react";
import { render } from "react-dom";
import App from "./App";
import { Provider } from "react-redux";
import {
  ThemeProvider,
  createTheme,
  Arwes,
  SoundsProvider,
  createSounds,
  Logo,
} from "arwes";
import store from "./store";
import { fetchCharacters } from "./redux/characters/actions";

const root = document.createElement("div");
document.body.append(root);

// Register service worker
if ("serviceWorker" in navigator) {
  window.addEventListener("load", () => {
    navigator.serviceWorker
      .register("/sw.js")
      .then((registration) => {
        console.log("SW registered:", registration);
      })
      .catch((error) => {
        console.log("SW registration failed:", error);
      });
  });
}

store.dispatch(fetchCharacters());

render(
  <Provider store={store}>
    <ThemeProvider theme={createTheme()}>
      <SoundsProvider sounds={createSounds()}>
        <App />
      </SoundsProvider>
    </ThemeProvider>
  </Provider>,
  root
);
