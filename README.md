# Invillia - Teste Front End

Demo: http://jittery-fan.surge.sh

[![](http://i.imgur.com/e5xCS1jr.png)]()
[![](http://i.imgur.com/62QYq7kr.png)]()
[![](http://i.imgur.com/7GyqZQ1.png)]()

## Candidato
---
Linkedin: https://www.linkedin.com/in/natan-dos-santos-camargos-7b8153122/

Github: https://github.com/thr0wn

Stack Overflow: https://stackoverflow.com/users/6489712/natan-camargos

## Comandos
---
### Rodar o projeto
```bash
npm install
npm start
```

### Buildar o projeto
```bash
npm install
npm build
```

## Observações
---
Foi necessário subir um servidor a parte, pois o swapi.co é muito instável!

Repositório: https://github.com/thr0wn/swapi-1

App Heroku: https://swapi-nsc.herokuapp.com/api/people

## Principais tecnologias:
---
- https://github.com/facebook/react (React: Biblioteca de componentes )
- https://github.com/styled-components/styled-components (Styled Components: Biblioteca para estilizar componentes)
- https://github.com/axios/axios (axios: HTTP client)
- https://github.com/webpack/webpack (Webpack: Empacotador de arquivos)
- https://github.com/babel/babel (Babel: Transpiler)
- https://github.com/eslint/eslint (ESlint: Linter)
- https://github.com/xojs/xo (xo: Linter defaults)
- https://github.com/zloirock/core-js (Core-js: Polyfill de funcionalidades)
- https://github.com/sindresorhus/modern-normalize (Modern-Normalize: Normaliza os estilos 'default' do browser)
